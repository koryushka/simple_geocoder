class Geocoder
  attr_reader :query, :error

  def initialize(query:)
    self.query = query
  end

  def run
    if successful?
      process_result
    else
      process_error
      false
    end
  end

  private

  attr_writer :query

  def request
    @request ||= HTTParty.get(url)
  end

  def successful?
    (200..300).cover?(request.code)
  end

  def google_api_key
    ENV.fetch('GOOGLE_API_KEY') do
      fail(ArgumentError, 'Set GOOGLE_API_KEY in the .env file')
    end
  end

  def google_url
    ENV.fetch('GOOGLE_API_URL') do
      fail(ArgumentError, 'Set GOOGLE_API_URL in the .env file')
    end
  end

  def url
    "#{google_url}?key=#{google_api_key}&address=#{query}"
  end

  def body
    @body ||= JSON.parse(request.body)
  end

  def process_error
    @error = begin
      body['error_message']
    rescue JSON::ParserError
      I18n.t('errors.unexpected')
    end
    Rails.logger.error("[Geocoder] - #{error}")
    false
  end

  def results
    body['results'][0]
  end

  def process_result
    return process_error if body['error_message'].present?
    return {} unless results
    results['geometry']['location']
  end
end
