class GeocoderController < ApplicationController
  before_action :validate_params, only: :index

  def index
    result = Rails.cache.fetch(params[:query]) { client.run }

    if result
      render json: { coordinates: result }, status: :ok
    else
      render json: { error: client.error }, status: :unpocessable_entity
    end
  end

  private

  def client
    @client ||= Geocoder.new(query: params[:query])
  end

  def validate_params
    render_error(message: I18n.t('errors.blank_query')) if params[:query].blank?
  end
end
