class ApplicationController < ActionController::API
  before_action :authenticate!

  rescue_from ArgumentError do |exception|
    Rails.logger.error(exception.message)
    render_error(message: I18n.t('errors.unexpected'), status: 422)
  end

  private

  def api_key
    ENV.fetch('API_KEY') do
      Rails.logger.error('Set API_KEY in the .env file')
      nil
    end
  end

  def authenticate!
    render_error(message: I18n.t('errors.not_permitted')) unless authorized?
  end

  def authorized?
    api_key && params[:api_key] == api_key
  end

  def render_error(message:, status: 422)
    render json: { error: message }, status: status
  end
end
