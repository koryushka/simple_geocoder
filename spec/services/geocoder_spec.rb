require 'rails_helper'

describe Geocoder do
  let(:query) { 'checkpoint charlie' }
  subject { described_class.new(query: query) }

  context 'success' do
    it 'returns coordinates' do
      VCR.use_cassette('geocode-success') do
        body = subject.run

        expect(body['lat']).to eq(52.5074434)
        expect(body['lng']).to eq(13.3903913)
      end
    end

    context 'address not found' do
      let(:query) { 'invalid_address' }
      it 'returns empty object' do
        VCR.use_cassette('geocode-invalid-address') do
          body = subject.run

          expect(body).to eq({})
        end
      end
    end
  end

  context 'failure' do
    context 'invalid Google API key' do
      it 'renders error message' do
        api_key = ENV.delete('GOOGLE_API_KEY')
        ENV['GOOGLE_API_KEY'] = 'invalid_api_key'
        VCR.use_cassette('geocode-failure-invalid-api-key') do
          subject.run

          expect(subject.error).to eq('The provided API key is invalid.')
        end
        ENV['GOOGLE_API_KEY'] = api_key
      end
    end

    context 'missing address' do
      let(:query) { '' }

      it 'returns false' do
        VCR.use_cassette('geocode-failure-missing-address') do
          expect(subject.run).to be_falsey
        end
      end

      it 'provides error message' do
        VCR.use_cassette('geocode-failure-missing-address') do
          subject.run

          expect(subject.error)
            .to eq("Invalid request. Missing the 'address', 'components',"\
              " 'latlng' or 'place_id' parameter.")
        end
      end

      it 'logs error' do
        VCR.use_cassette('geocode-failure-missing-address') do
          expect(Rails.logger)
            .to receive(:error).with('[Geocoder] - Invalid request. Missing '\
              "the 'address', 'components', 'latlng' or 'place_id' parameter.")
          subject.run
        end
      end
    end

    context 'Google API server error' do
      it 'returns false' do
        VCR.use_cassette('geocode-failure-internal-server-error') do
          expect(subject.run).to be_falsey
        end
      end

      it 'provides error' do
        VCR.use_cassette('geocode-failure-internal-server-error') do
          subject.run

          expect(subject.error)
            .to eq('Something went wrong, please try again later')
        end
      end
    end
  end

  context 'when environment variables are missing' do
    it 'throws an exception when GOOGLE_API_KEY is not set' do
      api_key = ENV.delete('GOOGLE_API_KEY')
      message = 'Set GOOGLE_API_KEY in the .env file'
      expect { subject.run }.to raise_error(ArgumentError, message)

      ENV['GOOGLE_API_KEY'] = api_key
    end

    it 'throws an exception when GOOGLE_API_URL is not set' do
      api_url = ENV.delete('GOOGLE_API_URL')
      message = 'Set GOOGLE_API_URL in the .env file'
      expect { subject.run }.to raise_error(ArgumentError, message)

      ENV['GOOGLE_API_URL'] = api_url
    end
  end
end
