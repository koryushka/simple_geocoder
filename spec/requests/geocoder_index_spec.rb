require 'rails_helper'

RSpec.describe 'GET /index' do
  context 'success' do
    before(:each) do
      VCR.use_cassette('geocode-success') do
        Rails.cache.clear
        get root_path(:json, params: params)
      end
    end

    let(:params) { { api_key: ENV['API_KEY'], query: 'checkpoint charlie' } }

    it 'returns 200 status code' do
      expect(response).to have_http_status(200)
    end

    it 'renders results' do
      body = JSON.parse(response.body)

      expect(body['coordinates']['lat']).to eq(52.5074434)
      expect(body['coordinates']['lng']).to eq(13.3903913)
    end

    context 'uses cached values' do
      it "doesn't call google API" do
        expect_any_instance_of(Geocoder).to_not receive(:run)

        get root_path(:json, params: params)
      end
    end

    context 'invalid address' do
      it 'renders empty object' do
        VCR.use_cassette('geocode-invalid-address') do
          params = { api_key: ENV['API_KEY'], query: 'invalid_address' }
          get root_path(:json, params: params)
          body = JSON.parse(response.body)

          expect(body['coordinates']).to eq({})
        end
      end

      it 'returns 200 status code' do
        expect(response).to have_http_status(200)
      end
    end
  end

  context 'failure' do
    context 'incorrect API KEY' do
      before(:each) do
        get root_path(:json, params: params)
      end
      let(:params) { { api_key: 'incorrect_key', query: 'checkpoint charlie' } }

      it 'returns 422 status code' do
        expect(response).to have_http_status(422)
      end

      it 'renders error' do
        body = JSON.parse(response.body)
        expect(body['error']).to eq('Operation not permitted')
      end
    end

    context 'API KEY is not set to env' do
      let!(:api_key) { ENV.delete('API_KEY') }
      before(:each) do
        Rails.cache.clear
        get root_path(:json, params: params)
      end
      let(:params) { { api_key: ENV['API_KEY'], query: 'checkpoint charlie' } }

      it 'returns 422 status code' do
        expect(response).to have_http_status(422)
        ENV['API_KEY'] = api_key
      end

      it 'renders error' do
        body = JSON.parse(response.body)
        expect(body['error']).to eq('Operation not permitted')
        ENV['API_KEY'] = api_key
      end

      it 'logs error' do
        expect(Rails.logger)
          .to receive(:error).with('Set API_KEY in the .env file')

        get root_path(:json, params: params)
        ENV['API_KEY'] = api_key
      end
    end

    context 'Google API KEY is not set to env' do
      let!(:google_api_key) { ENV.delete('GOOGLE_API_KEY') }
      before(:each) do
        Rails.cache.clear
        get root_path(:json, params: params)
      end
      let(:params) { { api_key: ENV['API_KEY'], query: 'checkpoint charlie' } }

      it 'returns 422 status code' do
        expect(response).to have_http_status(422)
        ENV['GOOGLE_API_KEY'] = google_api_key
      end

      it 'renders error' do
        body = JSON.parse(response.body)
        expect(body['error'])
          .to eq('Something went wrong, please try again later')
        ENV['GOOGLE_API_KEY'] = google_api_key
      end

      it 'logs error' do
        expect(Rails.logger)
          .to receive(:error).with('Set GOOGLE_API_KEY in the .env file')

        get root_path(:json, params: params)
        ENV['GOOGLE_API_KEY'] = google_api_key
      end
    end
  end
end
