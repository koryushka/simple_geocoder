# SimpleGeocoder
Simple app for converting string address to coordinates.

## Getting Started
Run `bin/setup` to setup application. `rspec` - for running tests, `rubocop` for code analyze.

### Environment Variables
This project uses [dotenv](https://github.com/bkeepers/dotenv) to set configuration values as environment variables. If you ran `bin/setup`, you should have both a `.env.development` and a `.env.test` file.

When adding configuration values to the project, first make an entry in `.env.example` . If the data is sensitive, such as passwords, use a placeholder:

```
SECRET_PASSWORD = :put_your_super_secret_password_here
```

If the data is not sensitive, like a URL, you can store it directly in `.env.example` and then commit it to git.

If you have environment specific variables, place these into their respective environments:

 * .env.example - example used by `bin/setup` to create the next two files
 * .env.development - loaded by development
 * .env.test - loaded by test


`GOOGLE_API_KEY` - google API key that is used for making calls to Google API.
`API_KEY` - key for making calls to application endpoint(s), could be any value.
